# #ilovefs19

Image created by [ElektrollArt](http://repo.elektrollart.org/Artworks/xcf%20and%20svg/ilovefs19%20\(CC-BY\)/)

Forked by [Se7h](https://framagit.org/thomasb22/ilovefs19)

### Software used for this version

* Inkscape 0.92.1
* Gimp 2.8.18

### License

CC0
